function [mu, sigma] = prediction_step(mu, sigma, u)
% Updates the belief concerning the robot pose according to the motion model,
% mu: 2N+3 x 1 vector representing the state mean
% sigma: 2N+3 x 2N+3 covariance matrix
% u: odometry reading (r1, t, r2)
% Use u.r1, u.t, and u.r2 to access the rotation and translation values


% TODO: Compute the new mu based on the noise-free (odometry-based) motion model
% Remember to normalize theta after the update (hint: use the function normalize_angle available in tools)


%   INPUT:
%   Previous Pose (previousPose)
%mu: 2N+3x1 vector representing the mean of the normal distribution
%sigma: (2N+3)x(2N+3) covariance matrix of the normal distribution
%u=data.timestep(t).odometry - time


 %   Compute the current noiseless pose from previous noiseless Pose
    %mu_expected(t,:)=estimateOdometryPose(b,mu_expected(t-1,:),ut)
    
    
    %   START
previousX=mu(1);
previousY=mu(2);
previousTheta=mu(3);

N=9;
fx_1=eye(3);
fx_2=zeros([3,2*N]);
Fx=horzcat(fx_1,fx_2);

%wt+delta(t)
dt=u.r1-u.r2;

va1=-u.t * sin(previousTheta)+ u.t*sin(previousTheta+dt);
va2=u.t * cos(previousTheta) - u.t*cos(previousTheta+dt);
va3=normalize_angle(previousTheta+dt);

mat1=[va1;va2;va3];

mu_b= Fx.' * mat1;

mu=mu+mu_b;



% TODO: Compute the 3x3 Jacobian Gx of the motion model

%Gxt=[1,0, -u.t*cos(previousTheta)+u.t*cos(previousTheta+dt); 0,1,-u.t*sin(previousTheta)+u.t*sin(previousTheta+dt);0,0,1];
Gxt=[0,0, -u.t*cos(previousTheta)+u.t*cos(previousTheta+dt); 0,0,-u.t*sin(previousTheta)+u.t*sin(previousTheta+dt);0,0,0];

Gt1=Fx.'*Gxt*Fx;
I=eye(21);

% TODO: Construct the full Jacobian G

Gt=I+Gt1;

% Motion noise
motionNoise = 0.1;
R3 = [motionNoise, 0, 0; 
     0, motionNoise, 0; 
     0, 0, motionNoise/10];
R = zeros(size(sigma,1));
R(1:3,1:3) = R3;


% TODO: Compute the predicted sigma after incorporating the motion noise

%sigma_bar
sigma=Gt*sigma*Gt.'+R;


end
