function [mu, sigma, observedLandmarks] = correction_step(mu, sigma, z, observedLandmarks)
% Updates the belief, i. e., mu and sigma after observing landmarks, according to the sensor model
% The employed sensor model measures the range and bearing of a landmark
% mu: 2N+3 x 1 vector representing the state mean.
% The first 3 components of mu correspond to the current estimate of the robot pose [x; y; theta]
% The current pose estimate of the landmark with id = j is: [mu(2*j+2); mu(2*j+3)]
% sigma: 2N+3 x 2N+3 is the covariance matrix
% z: struct array containing the landmark observations.
% Each observation z(i) has an id z(i).id, a range z(i).range, and a bearing z(i).bearing
% The vector observedLandmarks indicates which landmarks have been observed
% at some point by the robot.
% observedLandmarks(j) is false if the landmark with id = j has never been observed before.

% Number of measurements in this time step
m = size(z, 2);

% Z: vectorized form of all measurements made in this time step: [range_1; bearing_1; range_2; bearing_2; ...; range_m; bearing_m]
% ExpectedZ: vectorized form of all expected measurements in the same form.
% They are initialized here and should be filled out in the for loop below
Z = zeros(m*2, 1);
expectedZ = zeros(m*2, 1);

% Iterate over the measurements and compute the H matrix
% (stacked Jacobian blocks of the measurement function)
% H will be 2m x 2N+3
H = [];

for i = 1:m
	% Get the id of the landmark corresponding to the i-th observation
	landmarkId = z(i).id;
	% If the landmark is obeserved for the first time:
	if(observedLandmarks(landmarkId)==false)
		% TODO: Initialize its pose in mu based on the measurement and the current robot pose:
        olandmarkjx=mu(1)+z(i).range*cos(z(i).bearing+mu(3));
        olandmarkjy=mu(2)+z(i).range*sin(z(i).bearing+mu(3));
        mu(4)=olandmarkjx;
        mu(5)=olandmarkjy;
    end
    
        %olandmarkjx=z(i).range*cos(z(i).bearing);
        %olandmarkjy=z(i).range*sin(z(i).bearing);
        
    % TODO: Add the landmark measurement to the Z vector
    Z=[mu(4);mu(5)];

		% Indicate in the observedLandmarks vector that this landmark has been observed
		observedLandmarks(landmarkId) = true;

	% TODO: Use the current estimate of the landmark pose (x,y)
	% to compute the corresponding expected measurement in expectedZ:
        dx=mu(4)-mu(1);
        dy=mu(5)-mu(2);
        
       mxy=[dx;dy];
       q=mxy.'*mxy;
         
        rho=sqrt(q);
        phi=atan2(dy,dx)-mu(3);
       phi=normalize_angle(phi);
        Z_e=[rho;phi];     
    
   
    % TODO: Compute the Jacobian Hi of the measurement function h for this observation
    Hi_1=(1/q)*[-sqrt(q)*mu(4) -sqrt(q)*mu(5) 0 sqrt(q)*mu(4) sqrt(q)*mu(5)];
    Hi_2=(1/q)*[mu(5) -mu(4) -q -mu(5) mu(4)];
    Hi=[Hi_1;Hi_2];
    
    
    N=9;
    F=eye(3);
   F1=zeros([3,2*i-2]);
   F2=zeros([3,2]);
    F3=zeros([3,2*N-2*i]);
    
    F4=zeros([2 3]);
    F5=zeros([2,2*i-2]);
    F6=eye(2);
    F7=zeros([2,2*N-2*i]);
   
    M1=horzcat(F,F1,F2,F3);
    M2=horzcat(F4,F5,F6,F7);
	Mf=vertcat(M1,M2);
    
     Hi=Hi*Mf;
     
	% Augment H with the new Hi
    % H will be 2m x 2N+3
	%H = [H;Hi];
    H=Hi;
end
    


% TODO: Construct the sensor noise matrix Q
Q=eye(2);
Qt=0.01*Q;

% TODO: Compute the Kalman gain
 %   Calculate innovation (residula) covariance
    S= ((H)*(sigma)*(H.')+Qt);
    
    %   Calculate Kalman Gain (Kgain)
    Kgain=  ((sigma)*(H.')); %/S
    Kgain=Kgain*inv(S);
    disp('oi')
    
    % TODO: Compute the difference between the expected and recorded measurements.
    Z_diff=Z-Z_e;
    Z_diff(2)=normalize_angle(Z_diff(2));
    
    mu_bar=[dx;dy;phi;mu(4);mu(5)];
    mu_res=zeros([2*N-2,1]);
    mu_bar=vertcat(mu_bar,mu_res);
        
    

% Remember to normalize the bearings after subtracting!
% (hint: use the normalize_all_bearings function available in tools)

% TODO: Finish the correction step by computing the new mu and sigma.
% Normalize theta in the robot pose.

mu=mu_bar+Kgain*Z_diff;


sigma=(eye(21)-Kgain*H)*sigma;
%observedLandmarks=observedLandmarks;
end
