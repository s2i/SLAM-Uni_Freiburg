function plot_map(mu, sigma, landmarks, timestep, observedLandmarks, z, window)
    % Visualizes the state of the EKF SLAM algorithm.
    %
    % The resulting plot displays the following information:
    % - map ground truth (black +'s)
    % - current robot pose estimate (red)
    % - current landmark pose estimates (blue)
    % - visualization of the observations made at this time step (line between robot and landmark)

    %clf;
    hold on
    grid('on')
    L = struct2cell(landmarks); 
    
    plot(cell2mat(L(2,:)), cell2mat(L(3,:)), 'k+', 'markersize', 10, 'linewidth', 5);
    
    drawrobot(mu(1:3), 'r', 3, 0.3, 0.3);
    hold on
    xlim([-2, 12])
    ylim([-2, 12])
    %hold off

    if window
      figure(2);
      pause(0.1);
    else
        %figure(2, 'visible', 'off');
     % filename = sprintf('../plots/ekf_%03d.png', timestep);
     % print(filename, '-dpng');
    end
end
