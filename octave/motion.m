function [mov] = motion(x,m)

tx=x(1);
ty=x(2);
ang=normalize_angle(x(3));
mov = [cos(ang) -sin(ang) tx; sin(ang) cos(ang) ty; 0 0 1]*m.';

end