%
function [x] = motion_command(x, u)
% Updates the robot pose according to the motion model
% x: 3x1 vector representing the robot pose [x; y; theta]
% u: struct containing odometry reading (r1, t, r2).
% Use u.r1, u.t, and u.r2 to access the rotation and translation values

%TODO: update x according to the motion represented by u
xPos=x(1); %-1 pos
yPos=x(2); %-1 pos
ang = normalize_angle(x(3)); %-1 angle

trans=u.t;
rot1=u.r1;
rot2=u.r2;

xPos=xPos+trans*cos(ang+rot1);
yPos=yPos+trans*sin(ang+rot1);
ang=ang+rot1+rot2;

ang=normalize_angle(ang); %
xPosition = x(1);
yPosition = x(2);

%TODO: remember to normalize theta by calling normalize_angle for x(3)
angle = normalize_angle(x(3)); %normalization
x = [xPos,yPos,ang];

end
